$(document).ready(function() {

// Begin Packery
  var $container = $('.content');
  //Initialize Packery after all images have loaded, fixes overlap on load. 
  //Also have to include imagesloaded.min.js
  $('.content').imagesLoaded( function() {
    // images have loaded
    $container.packery({
    itemSelector: '.post'
    });

  });// End Packery

//prevents standalone mode(iOS) links from opening iOS safari, unless target="_blank"
	$(document).on('touchend click', 'a', function(e) {

	    if ($(this).attr('target') !== '_blank') {
	        e.preventDefault();
	        window.location = $(this).attr('href');
	    }

	});
	
});